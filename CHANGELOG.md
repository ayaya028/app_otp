## 1.0.0

* Initial release.

## 1.0.1

* Remove riverpod. Use ChangeNotifier instead.

## 1.0.2

* Fixes box width not work properly.

## 1.0.3

* Fixes focus not cleared when users tap on back-press button (Android).


## 1.0.4

* Fixes OTP values notifier not cleared after state disposed.
 
## 1.0.5

* Update flutter_keyboard_visibility version to 6.0.0
