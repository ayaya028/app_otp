// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'otp_box_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$OTPBoxState {
  List<String> get otpValues => throw _privateConstructorUsedError;
  bool get isFocus => throw _privateConstructorUsedError;

  /// Create a copy of OTPBoxState
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $OTPBoxStateCopyWith<OTPBoxState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $OTPBoxStateCopyWith<$Res> {
  factory $OTPBoxStateCopyWith(
          OTPBoxState value, $Res Function(OTPBoxState) then) =
      _$OTPBoxStateCopyWithImpl<$Res, OTPBoxState>;
  @useResult
  $Res call({List<String> otpValues, bool isFocus});
}

/// @nodoc
class _$OTPBoxStateCopyWithImpl<$Res, $Val extends OTPBoxState>
    implements $OTPBoxStateCopyWith<$Res> {
  _$OTPBoxStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of OTPBoxState
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? otpValues = null,
    Object? isFocus = null,
  }) {
    return _then(_value.copyWith(
      otpValues: null == otpValues
          ? _value.otpValues
          : otpValues // ignore: cast_nullable_to_non_nullable
              as List<String>,
      isFocus: null == isFocus
          ? _value.isFocus
          : isFocus // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$OTPBoxStateImplCopyWith<$Res>
    implements $OTPBoxStateCopyWith<$Res> {
  factory _$$OTPBoxStateImplCopyWith(
          _$OTPBoxStateImpl value, $Res Function(_$OTPBoxStateImpl) then) =
      __$$OTPBoxStateImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<String> otpValues, bool isFocus});
}

/// @nodoc
class __$$OTPBoxStateImplCopyWithImpl<$Res>
    extends _$OTPBoxStateCopyWithImpl<$Res, _$OTPBoxStateImpl>
    implements _$$OTPBoxStateImplCopyWith<$Res> {
  __$$OTPBoxStateImplCopyWithImpl(
      _$OTPBoxStateImpl _value, $Res Function(_$OTPBoxStateImpl) _then)
      : super(_value, _then);

  /// Create a copy of OTPBoxState
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? otpValues = null,
    Object? isFocus = null,
  }) {
    return _then(_$OTPBoxStateImpl(
      otpValues: null == otpValues
          ? _value._otpValues
          : otpValues // ignore: cast_nullable_to_non_nullable
              as List<String>,
      isFocus: null == isFocus
          ? _value.isFocus
          : isFocus // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$OTPBoxStateImpl implements _OTPBoxState {
  const _$OTPBoxStateImpl(
      {required final List<String> otpValues, required this.isFocus})
      : _otpValues = otpValues;

  final List<String> _otpValues;
  @override
  List<String> get otpValues {
    if (_otpValues is EqualUnmodifiableListView) return _otpValues;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_otpValues);
  }

  @override
  final bool isFocus;

  @override
  String toString() {
    return 'OTPBoxState(otpValues: $otpValues, isFocus: $isFocus)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$OTPBoxStateImpl &&
            const DeepCollectionEquality()
                .equals(other._otpValues, _otpValues) &&
            (identical(other.isFocus, isFocus) || other.isFocus == isFocus));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_otpValues), isFocus);

  /// Create a copy of OTPBoxState
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$OTPBoxStateImplCopyWith<_$OTPBoxStateImpl> get copyWith =>
      __$$OTPBoxStateImplCopyWithImpl<_$OTPBoxStateImpl>(this, _$identity);
}

abstract class _OTPBoxState implements OTPBoxState {
  const factory _OTPBoxState(
      {required final List<String> otpValues,
      required final bool isFocus}) = _$OTPBoxStateImpl;

  @override
  List<String> get otpValues;
  @override
  bool get isFocus;

  /// Create a copy of OTPBoxState
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$OTPBoxStateImplCopyWith<_$OTPBoxStateImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
