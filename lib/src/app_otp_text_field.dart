import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:logger/logger.dart';

import 'app_otp_text_field_controller.dart';
import 'otp_box.dart';
import 'otp_box_state.dart';

var logger = Logger();

typedef AppOTPTextFieldControllerCallback = void Function(
  AppOTPTextFieldController controller,
);

class AppOTPTextField extends StatefulWidget {
  /// Callback function, get called when user put in all OTP boxes.
  final ValueChanged<String> onCompleted;

  /// Callback function, get called when there are changes in any OTP boxes.
  final ValueChanged<String>? onChanged;

  /// Callback function, get called when the widget's state is init.
  final AppOTPTextFieldControllerCallback? onAppOTPTextFieldCreated;

  /// Number of the OTP Fields
  final int digits;

  /// Color of OTP box's border when focused
  final Color boxFocusBorderColor;

  /// Color of OTP box's border when unfocused
  final Color boxUnFocusBorderColor;

  /// Circular radius of OTP box's border
  final double boxBorderRadius;

  /// Thickness of OTP box's border
  final double boxBorderWidth;

  /// OTP box's height
  final double boxHeight;

  /// OTP box's width
  final double boxWidth;

  /// Decided whether to automatically open keyboard or not
  final bool autofocus;

  /// Manage the type of keyboard that shows up
  final TextInputType keyboardType;

  /// The style to use for the text being edited.
  final TextStyle boxOTPTextStyle;

  /// Show cursor on OTP box
  final bool showCursor;

  /// Color of cursor in OTP box
  final Color cursorColor;

  /// Cursor's height
  final double cursorHeight;

  /// Thickness of cursor
  final double cursorWidth;

  /// Cursor's blink interval
  final Duration cursorBlinkInterval;

  final bool enabled;

  const AppOTPTextField({
    required this.onCompleted,
    this.onAppOTPTextFieldCreated,
    this.digits = 6,
    this.boxFocusBorderColor = Colors.blue,
    this.boxUnFocusBorderColor = const Color(0xFFCCCCCC),
    this.boxBorderRadius = 12,
    this.boxBorderWidth = 1.6,
    this.boxHeight = 48,
    this.boxWidth = 48,
    this.autofocus = true,
    this.keyboardType = TextInputType.number,
    this.boxOTPTextStyle = const TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w500,
      color: Colors.black87,
    ),
    this.showCursor = true,
    this.cursorColor = Colors.blue,
    this.cursorHeight = 24,
    this.cursorWidth = 1.6,
    this.cursorBlinkInterval = const Duration(milliseconds: 300),
    this.onChanged,
    this.enabled = true,
    super.key,
  }) : assert(digits >= 1);

  @override
  State<AppOTPTextField> createState() => _AppOTPTextFieldState();
}

class _AppOTPTextFieldState extends State<AppOTPTextField> {
  final TextEditingController _textController = TextEditingController();
  final FocusNode _focusNode = FocusNode();

  late StreamSubscription<bool> _keyboardSubscription;

  final _boxStateNotifier = ValueNotifier<OTPBoxState>(
    const OTPBoxState(
      otpValues: [],
      isFocus: true,
    ),
  );

  @override
  void dispose() {
    _keyboardSubscription.cancel();
    super.dispose();
  }

  void log(String msg) {
    logger.log(Level.info, msg);
  }

  @override
  void initState() {
    super.initState();

    var keyboardVisibilityController = KeyboardVisibilityController();

    _textController.clear();

    _focusNode.addListener(() {
      _boxStateNotifier.value = _boxStateNotifier.value.copyWith(
        isFocus: _focusNode.hasFocus,
      );
    });
    _keyboardSubscription = keyboardVisibilityController.onChange.listen((
      bool visible,
    ) {
      if (visible) {
        _focusNode.requestFocus();
      } else {
        FocusManager.instance.primaryFocus?.unfocus();
      }
    });
    widget.onAppOTPTextFieldCreated?.call(
      AppOTPTextFieldController(
        controller: _textController,
        notifier: _boxStateNotifier,
        onChanged: widget.onChanged,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    int otpLength = widget.digits;

    return Stack(
      children: [
        SizedBox(
          height: widget.boxHeight,
          child: TextFormField(
            controller: _textController,
            enabled: widget.enabled,
            focusNode: _focusNode,
            autofocus: widget.autofocus,
            showCursor: false,
            keyboardType: widget.keyboardType,
            maxLength: widget.digits,
            onChanged: (values) {
              final List<String> temp = [];
              for (int i = 0; i < values.length; i++) {
                temp.add(values[i]);
              }

              _boxStateNotifier.value = _boxStateNotifier.value.copyWith(
                otpValues: temp,
              );

              widget.onChanged?.call(values);

              if (values.length == otpLength) {
                widget.onCompleted.call(values);
              }
            },
            style: const TextStyle(
              color: Colors.transparent,
            ),
            decoration: const InputDecoration(
              contentPadding: EdgeInsets.zero,
              border: InputBorder.none,
              helperStyle: TextStyle(color: Colors.transparent),
              labelStyle: TextStyle(color: Colors.transparent),
            ),
          ),
        ),
        Positioned.fill(
          child: Row(
            children: [
              for (int i = 0; i < otpLength; i++) ...[
                Expanded(
                  child: OTPBox(
                    boxNumber: i + 1,
                    notifier: _boxStateNotifier,
                    boxFocusBorderColor: widget.boxFocusBorderColor,
                    boxUnFocusBorderColor: widget.boxUnFocusBorderColor,
                    textController: _textController,
                    focusNode: _focusNode,
                    enable: widget.enabled,
                    boxHeight: widget.boxHeight,
                    boxWidth: widget.boxWidth,
                    boxBorderWidth: widget.boxBorderWidth,
                    boxBorderRadius: widget.boxBorderRadius,
                    boxOTPTextStyle: widget.boxOTPTextStyle,
                    showCursor: widget.showCursor,
                    cursorWidth: widget.cursorWidth,
                    cursorHeight: widget.cursorHeight,
                    cursorColor: widget.cursorColor,
                    cursorBlinkInterval: widget.cursorBlinkInterval,
                  ),
                ),
              ],
            ],
          ),
        )
      ],
    );
  }
}
