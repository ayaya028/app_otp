import 'package:flutter/material.dart';

import 'otp_box_cursor.dart';
import 'otp_box_state.dart';

class OTPBox extends StatelessWidget {
  final ValueNotifier<OTPBoxState> notifier;
  final int boxNumber;
  final Color boxFocusBorderColor;
  final Color boxUnFocusBorderColor;
  final TextEditingController textController;
  final FocusNode focusNode;
  final bool enable;
  final double boxHeight;
  final double boxWidth;
  final double boxBorderWidth;
  final double boxBorderRadius;
  final TextStyle boxOTPTextStyle;
  final bool showCursor;
  final double cursorWidth;
  final double cursorHeight;
  final Color cursorColor;
  final Duration cursorBlinkInterval;

  const OTPBox({
    Key? key,
    required this.notifier,
    required this.boxNumber,
    required this.boxFocusBorderColor,
    required this.boxUnFocusBorderColor,
    required this.textController,
    required this.focusNode,
    required this.enable,
    required this.boxHeight,
    required this.boxWidth,
    required this.boxBorderWidth,
    required this.boxBorderRadius,
    required this.boxOTPTextStyle,
    required this.showCursor,
    required this.cursorWidth,
    required this.cursorHeight,
    required this.cursorColor,
    required this.cursorBlinkInterval,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      builder: (context, child) {
        var shouldShowCursor = false;
        final otpValues = notifier.value.otpValues;
        final isFocus = notifier.value.isFocus;
        final String boxValue;
        Color borderColor;

        if (otpValues.length >= boxNumber) {
          boxValue = otpValues[boxNumber - 1];
          borderColor = boxFocusBorderColor;
        } else {
          boxValue = '';
          borderColor = boxUnFocusBorderColor;
        }

        if (isFocus) {
          if (boxNumber == 1 && otpValues.isEmpty) {
            shouldShowCursor = true;
            borderColor = boxFocusBorderColor;
          } else if (textController.selection.base.offset == boxNumber) {
            shouldShowCursor = true;
          } else {
            shouldShowCursor = false;
          }
        } else {
          shouldShowCursor = false;
        }
        return Container(
          alignment: Alignment.center,
          child: GestureDetector(
            onTap: () {
              if (enable) {
                focusNode.requestFocus();
              }
            },
            child: Container(
              width: boxWidth,
              height: boxHeight,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(
                  color: borderColor,
                  width: boxBorderWidth,
                ),
                borderRadius: BorderRadius.circular(
                  boxBorderRadius,
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    boxValue,
                    style: boxOTPTextStyle,
                  ),
                  if (showCursor && shouldShowCursor) ...[
                    OTPBoxCursor(
                      cursorColor: cursorColor,
                      cursorHeight: cursorHeight,
                      cursorWidth: cursorWidth,
                      blinkInterval: cursorBlinkInterval,
                    ),
                  ],
                ],
              ),
            ),
          ),
        );
      },
      animation: notifier,
    );
  }
}
