import 'package:freezed_annotation/freezed_annotation.dart';

part 'otp_box_state.freezed.dart';

@Freezed(
  fromJson: false,
  toJson: false,
)
class OTPBoxState with _$OTPBoxState {
  const factory OTPBoxState({
    required List<String> otpValues,
    required bool isFocus,
  }) = _OTPBoxState;
}
