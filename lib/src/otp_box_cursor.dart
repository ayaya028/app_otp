import 'package:flutter/material.dart';

class OTPBoxCursor extends StatefulWidget {
  final double cursorHeight;
  final double cursorWidth;
  final Color cursorColor;
  final Duration blinkInterval;

  const OTPBoxCursor({
    required this.cursorHeight,
    required this.cursorWidth,
    required this.cursorColor,
    required this.blinkInterval,
    super.key,
  });

  @override
  State<StatefulWidget> createState() => _OTPBoxCursorState();
}

class _OTPBoxCursorState extends State<OTPBoxCursor>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: widget.blinkInterval,
    );
    _controller.repeat(reverse: true);
  }

  @override
  Widget build(BuildContext context) {
    return FadeTransition(
      opacity: _controller,
      child: Container(
        height: widget.cursorHeight,
        width: widget.cursorWidth,
        color: widget.cursorColor,
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
