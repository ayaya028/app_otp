import 'package:flutter/material.dart';

import 'otp_box_state.dart';

class AppOTPTextFieldController {
  final TextEditingController controller;
  final ValueNotifier<OTPBoxState> notifier;
  final ValueChanged<String>? onChanged;

  const AppOTPTextFieldController({
    required this.controller,
    required this.notifier,
    required this.onChanged,
  });

  void clear() {
    controller.clear();
    notifier.value = notifier.value.copyWith(
      otpValues: const [],
    );
    onChanged?.call('');
  }
}
