<!--
A simple OTP widget for flutter
-->

## Usage

```dart
import 'package:app_otp/app_otp.dart';

AppOTPTextField _buildOTPWidget() {
    return AppOTPTextField(
        onCompleted: (otp) {
            setState(() {
            _otpCompleted = otp;
            });
        },
        onChanged: (otp) {
            setState(() {
            _otp = otp;
            });
        },
    );
}
```





