import 'package:app_otp/app_otp.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(
    const MainApp(),
  );
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Builder(
        builder: (context) {
          return Container(
            color: Colors.white,
            child: Center(
              child: TextButton(
                child: const Text('Navigate To OTP'),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const Home(),
                    ),
                  );
                },
              ),
            ),
          );
        }
      ),
    );
  }
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  String? _otp;
  String? _otpCompleted;
  bool _enable = true;
  AppOTPTextFieldController? _controller;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('APP OTP example'),
        ),
        body: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 24,
                ),
                child: AppOTPTextField(
                  boxHeight: 48,
                  boxWidth: 40,
                  enabled: _enable,
                  onAppOTPTextFieldCreated: (controller) {
                    _controller = controller;
                  },
                  onCompleted: (otp) {
                    setState(() {
                      _otpCompleted = otp;
                    });
                  },
                  onChanged: (otp) {
                    setState(() {
                      _otp = otp;
                    });
                  },
                ),
              ),
              const SizedBox(
                height: 24,
              ),
              ElevatedButton(
                onPressed: () {
                  _controller?.clear();
                },
                child: const Text('Clear'),
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: _enable ? Colors.red : Colors.blue,
                ),
                onPressed: () {
                  setState(() {
                    _enable = !_enable;
                  });
                },
                child: Text(_enable ? 'disable' : 'enable'),
              ),
              const SizedBox(
                height: 24,
              ),
              Text('enable: $_enable'),
              _otpCompleted != null
                  ? Text(
                'onCompleted: $_otpCompleted',
              )
                  : const SizedBox(),
              _otp != null
                  ? Text(
                'onChanged: $_otp',
              )
                  : const SizedBox(),
            ],
          ),
        ),
      ),
    );
  }
}
